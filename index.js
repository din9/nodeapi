const mysql = require('mysql');
const express = require('express');
var app = express();
const bodyparser = require('body-parser');

app.use(bodyparser.json());



var mysqlConnection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'nodecrudmysql' 

});


mysqlConnection.connect((err) => {
	if (!err)
		console.log('DB Connection succeded.');
	else
		console.log('DB Connection failed \n Error : ' + JSON.stringify(err, undefined, 2));
});


app.listen(3000, ()=>console.log('Express server is running at port no : 3000'));




//SHOW
app.get('/employee', (req, res) => {
	mysqlConnection.query('Select * from Employee', (err, rows, fields)=> {
	if (!err)
		// console.log(rows);
		res.send(rows);
	else
		console.log(err);
	})
});


//UPDATE
app.get('/employee/:id', (req, res) => {
	mysqlConnection.query('Select * from Employee where EmpID = ?',[req.params.id], (err, rows, fields)=> {
	if (!err)
		// console.log(rows);
		res.send(rows);
	else
		console.log(err);
	})
});


//DELETE
app.delete('/employee/:id', (req, res) => {
	mysqlConnection.query('Delete Employee where EmpID = ?',[req.params.id], (err, rows, fields)=> {
	if (!err)
		// console.log(rows);
		res.send('Deleted Successfully');
	else
		console.log(err);
	})
});



//INSERT
app.post('/employee', (req, res) => {
	mysqlConnection.query('insert into Employee (Name, EmpCode, Salary) values (?,?,?)', [req.body.Name, req.body.EmpCode, req.body.Salary], (err, rows, fields) =>{ 	
		if (!err)
		// console.log(rows);
		res.send('Insert Successfully');
	else
		console.log(err);
	})
});


//UPDATE
app.put('/employee', (req, res) => {
	mysqlConnection.query('update Employee set Name = ?, EmpCode = ?, Salary = ? where EmpID = ?', [req.body.Name, req.body.EmpCode, req.body.Salary, req.body.EmpID], (err, rows, fields) =>{ 	
		if (!err)
		// console.log(rows);
		res.send('Update Successfully');
	else
		console.log(err);
	})
});
